export const addTodo = (input) => {
    return {
        type: 'ADD_TODO',
        payload: input
    }
}

export const removeTodo = (id) => {
    return {
        type: 'REMOVE_TODO',
        payload: id
    }
}

export const toggleTodo = (id) => {
    return {
        type: 'TOGGLE_TODO',
        payload: id
    }
}
