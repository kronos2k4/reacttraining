import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, Link, browserHistory } from 'react-router'
import {applyMiddleware, createStore} from 'redux'
import logger from 'redux-logger'
import {Provider} from 'react-redux'
import allReducers from './reducers'

import Boards from './components/Boards'
import Tracker from './components/Tracker'

const middleWare = applyMiddleware(logger());

const store = createStore(allReducers, middleWare);

//Youtube API key
// const API_KEY = 'AIzaSyD58enUKxLLl83gmaf3VGRA55Xvsb4CsSs'

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={Tracker} />
            <Route path="/boards" component={Boards} />
            {/*<Route path="chat" component={Chat} />*/}
        </Router>
    </Provider>
     ,document.getElementById('app')
);

