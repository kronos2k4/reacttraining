import React, {Component} from 'react'
import Table from '../Table'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {addTodo} from '../../actions'


import Todo from './Components/Todo/'

import './pagebody.sass'
import './todos.sass'

@connect(function (state) {
    return {
        todos: state.todos,
    }
})
export default class PageContent extends Component {

    static propTypes = {}

    state = {
        inputValue: "",
        isPressed: false,
    }

    render() {

        return (
            <section className="b-page_content">

                <Table />

                <p>Todos:</p>

                {this.renderTodos()}

                <input type="text" value={this.state.inputValue} onChange={this.handleValue}/>

                <button onClick={this.createTodo}>create</button>

            </section>
        )
    }

    createTodo = () => {
        this.props.dispatch(addTodo(this.state.inputValue))
        this.state.inputValue = "";
    }



    renderTodos() {
        return (
            this.props.todos.map((todo) => {
                    return (
                        <div>
                            <Todo todo={todo}/>
                        </div>
                    )
                }
            )
        )
    }

    handleValue = (e) => {
        this.setState({
            inputValue: e.target.value
        })
    }
}

// function mapStateToProps(state){
//     return{
//         todos: state.todos,
//         addTodo: state.addTodo
//     }
// }

// function matchDispatchToProps(dispatch){
//     return bindActionCreators({ addTodo: addTodo }, dispatch)
// }

// export default connect(mapStateToProps, matchDispatchToProps)(PageContent)
export default PageContent