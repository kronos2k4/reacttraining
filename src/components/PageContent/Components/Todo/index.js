import React from 'react'
import {Component} from 'react'
import classNames from 'classnames'
import {removeTodo, toggleTodo} from './../../../../actions/'
import {connect} from 'react-redux'

@connect()
export default class Todo extends Component {

    static propTypes = {
        todo: React.PropTypes.any.isRequired,
    }

    state = {
    }

    render() {
        const className = classNames({
            'inline': true,
        })
        return (
            <div>
                <input
                    type="checkbox"
                    onClick={this._toggleTodo}
                />

                <div
                    className={className}
                    key={this.props.todo.id}
                >
                    {this.props.todo.task}
                </div>

                <button onClick={this.delTodo}>
                    del
                </button>
            </div>
        )
    }

    delTodo = () => {
        this.props.dispatch(removeTodo(this.props.todo.id));
    }

    _toggleTodo = () => {
        this.props.dispatch(toggleTodo(this.props.todo.id));
    }
}