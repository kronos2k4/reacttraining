import React, {Component} from 'react'
import classNames from 'classnames'

import './button.sass'

export default class Button extends Component {
    render(){

        const className = classNames({
            'b-button': true,
            'b-button_icon--chat': this.props.icon == 'chat',
            'b-button_icon--tracker': this.props.icon == 'tracker',
            'b-button_icon--boards': this.props.icon == 'boards'
        })

        return (
            <button className={className}>
                {this.renderIcon()}
                {this.props.text}
            </button>
        )
    }

    renderIcon = () => {
        if(this.props.icon){
            return (
                <div className="b-button--icon">

                </div>
            )
        } else {
            return null
        }

    }
}


