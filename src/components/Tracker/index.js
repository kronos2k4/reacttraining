import React from 'react'
import Header from 'components/Header'
import PageContent from '../PageContent'
import Footer from 'components/Footer'

import 'sass/reset'
import 'sass/core'

const App = () => (
    <div>
        <Header />
        <PageContent />
        <Footer />
    </div>
)

export default App