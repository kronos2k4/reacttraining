import React, {Component} from 'react'
import Logo from './Components/Logo'
import UserPanel from './Components/UserPanel'
import Tabs from './Components/Tabs'
import Search from './Components/Search'
import Notifications from './Components/Notifications'

import './header.sass'

export default class Header extends Component {
    render(){
        return (
            <header className="b-header">
                <Tabs />
                <Search />
                <Logo />
                <UserPanel />
                <Notifications />
            </header>
        )
    }
}



