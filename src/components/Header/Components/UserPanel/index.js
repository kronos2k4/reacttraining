import React, {Component} from 'react'
import UserInfo from './Components/UserInfo'
import Login from './Components/Login'
import Logout from './Components/Logout'

import './userpanel.sass'

export default class UserPanel extends Component {
    render(){
        return (
            <div className="b-userPanel">
                <UserInfo />
                <Login />
                <Logout />
            </div>
        )
    }
}



