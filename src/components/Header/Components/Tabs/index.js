import React, {Component} from 'react'
import Button from 'components/Reusable/Button'
import {Link} from 'react-router'

import './tabs.sass'

export default class Header extends Component {
    render(){
        return (
            <div className="b-tabs">
                <Link to={`/chat`} >
                    <Button text="Chat" icon="chat" />
                </Link>
                <Link to={`/`} >
                    <Button text="Tracker" icon="tracker"/>
                </Link>
                <Link to={`/boards`} >
                    <Button text="Boards" />
                </Link>
            </div>
        )
    }
}



