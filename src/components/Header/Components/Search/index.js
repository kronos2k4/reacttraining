import React, {Component} from 'react'

import './search.sass'

export default class Search extends Component {
    render(){
        return (
            <div className="b-search">
                <input type="text" name="search" placeholder="Search.." />
            </div>
        )
    }
}



