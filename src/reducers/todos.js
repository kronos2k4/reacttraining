import _ from 'lodash'


const initialTodos = [
]

let id = 0;
export default function (todos = initialTodos, action) {
    switch (action.type) {
        case 'ADD_TODO':
            return [
                ...todos,
                {
                    id: id++,
                    task: action.payload,
                    done: false
                }
            ]

        case 'TOGGLE_TODO':
            console.log(action.payload)

            return _.map(todos, (todo) => {
                if (todo.id == action.payload) {
                    return {
                        ...todo,
                        done: !todo.done
                    }
                }
                return todo
            })

        case 'REMOVE_TODO':
            return todos.filter((todo) => todo.id != action.payload)
    }

    return todos
}