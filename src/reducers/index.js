import {combineReducers} from 'redux'
import Todos from './todos'
import ReducerAddTodo from './reducerAddTodo'

const allReducers = combineReducers({
    todos: Todos,
    addTodo: ReducerAddTodo
});

export default allReducers