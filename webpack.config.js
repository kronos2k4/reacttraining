'use strict'

var path = require('path')
var webpack = require('webpack')
var ExtractTextPlugin = require("extract-text-webpack-plugin")
var autoprefixer = require('autoprefixer')


//Сюда складываются выдернутые стили
var extractCSS = new ExtractTextPlugin('/css/[name].css');

module.exports = {
    //Настройки локального сервера
    devServer: {
        inline: true,
        contentBase: './build',
        port: 3001
    },
    watchOptions: {
        /**
         * WebStorm часто сохраняет много файлов одновременно,
         * и чтобы компиляция не запускалась множество раз, мы
         * указываем этот параметр.
         *
         * @see {@link https://webpack.github.io/docs/configuration.html#watchoptions-aggregatetimeout}
         */
        aggregateTimeout: 100,
    },

    //Входная точка приложения
    entry: {
        app: './src/index.js'
    },

    //Выходной скомпилированный файл, [name] будет взят из entry
    output: {
        path: path.join(__dirname, 'build'),
        publicPath: '/',
        filename: 'bundle/[name].bundle.js'
    },

    //Работа с расширениями
    module: {
        loaders: [

            //Настройки бабеля в .babelrc
            {
                test: /\.js$/,
                loaders: ['babel'],
                exclude: /node_modules/
            },
            {
                test: /\.sass$/,
                //Вытащит скомпилированный цсс из bundle
                loader: extractCSS.extract('style', ['css','sass'])
            },
        ]
    },

    //Префиксер
    postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ],

    //На какие расширения обращать внимание
    resolve: {
        extensions: ['', '.js', '.sass', '.json'],
        modulesDirectories: ['node_modules'],

        //Папки для быстрого доступа через import, например
        alias: {
            components: path.resolve(__dirname, 'src/components'),
            sass: path.resolve(__dirname, 'src/sass')
        }
    },

    //Плагины
    plugins: [
       extractCSS,
    ]
};